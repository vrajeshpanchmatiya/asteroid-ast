import { asteroidType } from "./Type/asteroidType";
import { getasteroidService } from "../Service/getasteroidService";
export const asteroidAction = (id) => {
  return async (dispatch) => {
    const details = await getasteroidService(id);
    dispatch({ type: asteroidType, payload: details.data });
  };
};
