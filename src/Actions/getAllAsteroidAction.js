import { getAsteroidAll } from "../Service/getAsteroidAll";
// Action for calling an Api
export const getAllAsteroidAction = async () => {
  const details = await getAsteroidAll();
  return details.data;
};
