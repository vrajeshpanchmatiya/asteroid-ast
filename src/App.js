import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import AsteroidForm from "./Components/AsteroidForm";
import AsteroidDetail from "./Components/AsteroidDetail";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={AsteroidForm} />
          <Route path="/AsteroidDetail" component={AsteroidDetail} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}
export default App;
