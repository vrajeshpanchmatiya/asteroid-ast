import { asteroidType } from "../Actions/Type/asteroidType";
const initstate = {
  data: [],
};
export const asteroidReducer = (state = initstate, { type, payload }) => {
  switch (type) {
    case asteroidType:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};
