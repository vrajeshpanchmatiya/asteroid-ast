import Axios from "axios";

export const getAsteroidAll = () => {
  return Axios.get(`${process.env.REACT_APP_API_RANDOM}`);
};
