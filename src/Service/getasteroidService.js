import Axios from "axios";

export const getasteroidService = (id) => {
  return Axios.get(
    `${process.env.REACT_APP_API_ASTEROID}${id}?${process.env.REACT_APP_API_KEY}`
  );
};
