import { Button, CardContent, TextField, Card } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { asteroidAction } from "../Actions/asteroidAction";
import { getAllAsteroidAction } from "../Actions/getAllAsteroidAction";
import { Link } from "react-router-dom";
const AsteroidForm = () => {
  const [id, setId] = useState(null);
  const [allAsteroid, setAllAsteroid] = useState([]);
  const dispatch = useDispatch();
  useEffect(async () => {
    const data = await getAllAsteroidAction();
    setAllAsteroid(data.near_earth_objects);
  }, []);
  const changeId = (e) => {
    setId(e.target.value);
  };
  const submitAsteroid = () => {
    dispatch(asteroidAction(id));
  };
  const RandomAsteroid = async () => {
    const id = allAsteroid.map(({ id }) => id);
    const number = Math.ceil(Math.abs(Math.random() * id.length));
    dispatch(asteroidAction(id[number]));
  };
  return (
    <div>
      <Card>
        <CardContent>
          <TextField name={id} onChange={changeId} placeholder="Asteroid Id" />
          <Link to={{ pathname: "/AsteroidDetail" }}>
            <Button type="submit" onClick={submitAsteroid}>
              Submit Asteroid
            </Button>
          </Link>
          <Link to={{ pathname: "/AsteroidDetail" }}>
            <Button type="submit" onClick={RandomAsteroid}>
              Random Asteroid
            </Button>
          </Link>
        </CardContent>
      </Card>
    </div>
  );
};
export default AsteroidForm;
