import { Card, CardContent, Typography } from "@material-ui/core";
import React from "react";
import { useSelector } from "react-redux";
const AsteroidDetail = () => {
  const info = useSelector((state) => {
    return state.data;
  });
  return (
    <div>
      <Card>
        <CardContent>
          <Typography>
            <b>Name</b>
            {info.name}
          </Typography>
          <Typography>
            <b>Nasa_Jpl_Url</b>
            {info.nasa_jpl_url}
          </Typography>
          <Typography>
            <b>Is_Potentially_Hazardous_Asteroid</b>
            {info.is_potentially_hazardous_asteroid ? "true" : "false"}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
};
export default AsteroidDetail;
